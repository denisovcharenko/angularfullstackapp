import { Request, Response } from "express";
import { BookController } from "../controllers/bookController";
import { AuthorController } from "../controllers/authorController";
import { AuthMiddleware } from "../middlewares/authMiddleware";
import { AuthController } from "../controllers/authController";
import { UserController } from "../controllers/userController";
import { MagazineController } from "../controllers/magazineController";
import { userRole } from "../../../shared/models/user-role.model";
export class Routes {
  public bookController: BookController = new BookController();
  public authorController: AuthorController = new AuthorController();
  public authController: AuthController = new AuthController();
  public userController: UserController = new UserController();
  public magazineController: MagazineController = new MagazineController();

  public routes(app): void {
    app.route("/").get((req: Request, res: Response) => {
      res.status(200).send({
        message: "GET request successfulll!!!!"
      });
    });

    // Auth
    app
      .route("/auth/profile/:_id")
      .get(
        AuthMiddleware([userRole.admin, userRole.user]),
        this.userController.getUserById
      )
      .put(AuthMiddleware([userRole.admin]), this.userController.updateUser)
      .delete(AuthMiddleware([userRole.admin]), this.userController.deleteUser);
    app
      .route("/auth/profile")
      .get(this.userController.getUsersProfiles)
      .post(AuthMiddleware([userRole.admin]), this.userController.addUser);
    app.route("/auth/register").post(this.authController.register);
    app.route("/auth/login").post(this.authController.login);

    // Book
    app
      .route("/book")
      .get(this.bookController.getBook)
      .post(this.bookController.addBook);
    app
      .route("/book/search")
      .post(
        AuthMiddleware([userRole.admin, userRole.user]),
        this.bookController.searchBookByTitle
      );
    // app.route('/book/:page')
    // 	.post(AuthMiddleware([userRole.admin, userRole.user]), this.bookController.searchBookByTitle)
    app
      .route("/book/:bookId")
      .get(AuthMiddleware([userRole.admin]), this.bookController.getBookById)
      .put(AuthMiddleware([userRole.admin]), this.bookController.updateBook)
      .delete(AuthMiddleware([userRole.admin]), this.bookController.deleteBook);
    app
      .route("/book/author/:authorId")
      .get(
        AuthMiddleware([userRole.admin]),
        this.bookController.getBookByAuthorId
      );

    // Author
    app
      .route("/author")
      .get(this.authorController.getAuthor)
      .post(this.authorController.addAuthor);
    app
      .route("/author/:authorId")
      .get(this.authorController.getAuthorById)
      .put(this.authorController.updateAuthor)
      .delete(this.authorController.deleteAuthor);

    // Magazine
    app
      .route("/magazine")
      .get(this.magazineController.getMagazine)
      .post(this.magazineController.addMagazine);
    app
      .route("/magazine/search")
      .post(
        AuthMiddleware([userRole.admin, userRole.user]),
        this.magazineController.searchMagazineByTitle
      );
    app
      .route("/magazine/:id")
      .get(this.magazineController.getMagazineById)
      .put(this.magazineController.updateMagazine)
      .delete(this.magazineController.deleteMagazine);
  }
}
