import { Response } from "express";
import { RequestModel } from "../middlewares/authMiddleware";
import { BookRepository } from "../repositories/bookRepository";

export class BookController {
  public searchBookByTitle(req: RequestModel<{}>, res: Response) {
    console.log(req.body);
    let reg = new RegExp(".*" + req.body.title + ".*", "i");
    // let regg = new RegExp('.*'+req.body.authorId+'.*', 'i');
    BookRepository.aggregate([
      {
        $lookup: {
          from: "authors",
          localField: "authorId",
          foreignField: "_id",
          as: "authorId"
        }
      },
      {
        $lookup: {
          from: "magazines",
          localField: "magazine",
          foreignField: "_id",
          as: "magazine"
        }
      },
      { $match: { title: reg } },
      { $limit: 10 }
    ]).then(book => [res.json(book)]);
  }

  public getBook(req: RequestModel<{}>, res: Response) {
    console.log(req.query);
    // const pageSize = +req.query.pagesize;
    // const currentPage = +req.query.page;
    // let count = +req.query.totalBooks;
    // let skip = pageSize * (currentPage - 1) || 0;

    BookRepository.aggregate([
      {
        $lookup: {
          from: "authors",
          localField: "authorId",
          foreignField: "_id",
          as: "authorId"
        }
      },
      {
        $lookup: {
          from: "magazines",
          localField: "magazine",
          foreignField: "_id",
          as: "magazine"
        }
      }

      // { $skip: skip },

      // { $limit: pageSize }
    ]).then(book => {
      // console.log(skip, req.query.pagesize, count);
      console.log(book);
      res.json(book);
    });
  }

  public getBookById(req: RequestModel<{ bookId: string }>, res: Response) {
    console.log(req.params);
    BookRepository.findById(req.params.bookId, (err, book) => {
      if (err) return res.status(500).send("Error on the server.");
      res.json(book);
    });
  }

  public getBookByAuthorId(
    req: RequestModel<{ authorId: string }>,
    res: Response
  ) {
    let authorId = req.params.authorId;
    BookRepository.find({ authorId: authorId }, (err, book) => {
      if (err) return res.status(500).send("Error on the server.");
      res.json(book);
    });
  }

  public addBook(req: RequestModel<{}>, res: Response) {
    console.log(req.body);
    let newBook = new BookRepository(req.body);
    newBook.save((err, book) => {
      if (err) return res.status(500).send("Error on the server.");
      res.status(200).send({ book: book });
    });
  }

  public updateBook(req: RequestModel<{ bookId: string }>, res: Response) {
    BookRepository.findOneAndUpdate(
      { _id: req.params.bookId },
      req.body,
      { new: true },
      (err, contact) => {
        if (err) return res.status(500).send("Error on the server.");
        res.json(contact);
      }
    );
  }

  public deleteBook(req: RequestModel<{ bookId: string }>, res: Response) {
    BookRepository.remove({ _id: req.params.bookId }, err => {
      if (err) return res.status(500).send("Error on the server.");
      res.json({ message: "Successfully deleted contact!" });
    });
  }
}
