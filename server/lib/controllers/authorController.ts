import { Response } from 'express';
import { RequestModel, RequestPost } from '../middlewares/authMiddleware'
import { AuthorRepository } from '../repositories/authorRepository';
import { BookRepository } from '../repositories/bookRepository';
import { MagazineRepository } from '../repositories/magazineRepository';

export class AuthorController{

	public getAuthor (req: RequestModel<{}>, res: Response) {
		AuthorRepository.find({}, (err, author) => {
			if (err) return res.status(500).send('Error on the server.');
			res.json(author);
		});
	}

	public getAuthorById (req: RequestModel<{authorId:string}>, res: Response) {           
		AuthorRepository.findById(req.params.authorId, (err, author) => {
			if (err) return res.status(500).send('Error on the server.');
			res.json(author);
		});
	}

    public addAuthor (req: RequestPost<{fullName: string}>, res: Response) {
		let newAuthor = new AuthorRepository({fullName: req.body.fullName});
		// console.log(newAuthor);
        newAuthor.save((err, author) => {
            if (err) return res.status(500).send('Error on the server.');
			res.status(200).send({_id: author._id, fullName: author.fullName})
        });
    }


    public updateAuthor (req: RequestModel<{authorId:string}>, res: Response) {           
        AuthorRepository.findOneAndUpdate({ _id: req.params.authorId }, req.body, { new: true }, (err, author) => {
            if (err) return res.status(500).send('Error on the server.');
            res.json(author);
        });
    }

    public async deleteAuthor (req: RequestModel<{authorId:string}>, res: Response) {
		await BookRepository.update({authorId: req.params.authorId},
			{$pull: {authorId: req.params.authorId}}, 
			{multi: true});
		await MagazineRepository.update({authorId: req.params.authorId},
			{$pull: {authorId: req.params.authorId}}, 
			{multi: true});
			
			AuthorRepository.remove({ _id: req.params.authorId }, (err) => {
				if (err) return res.status(500).send('Error on the server.');
				res.json({ message: 'Successfully deleted contact!'});
		});
	}
}