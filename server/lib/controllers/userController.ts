import { RequestPost, RequestModel } from "../middlewares/authMiddleware";
import { Response } from "express";
import { UserRepository } from "../repositories/userRepository";
import * as bcryptjs from 'bcryptjs';

export class UserController {

	public getUsersProfiles(req: RequestModel<{}>, res: Response) {
		UserRepository.find({}, (err, user) => {
			res.send(user);
		})
	}
	
	public getUserById(req:RequestModel<{_id: string}>, res: Response) {
		UserRepository.findById(req.params._id, (err, user) => {
			if (err) return res.status(500).send('Error on the server.');
			res.json(user);
		})
	}
	
	public addUser(req: RequestPost<{fullName: string, email: string, password: string, userRole: number}>, res: Response) {
		var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
  
		UserRepository.create({
			fullName: req.body.fullName,
			email: req.body.email,
			password: hashedPassword,
			userRole: req.body.userRole,
			confirm: false
		},
		(err, user) => {
			if(err) return res.status(500).send("There was a problem adding the user.")
  
			res.status(200).send({user: user})
		});
	}

	public updateUser(req: RequestPost<{ _id: string, fullName: string, email: string, password: string, newPassword:string}>, res: Response) {
		// var passwordIsValid = bcryptjs.compareSync(req.body.password, user.password);
		var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
		// var hashedPassword = req.body.password
		// if(req.body.password === hashedPassword) {
		// 	var hashedPassword = req.body.password
		// } 
		// 	var hashedPassword = bcryptjs.hashSync(req.body.password, 8);
		// 	console.log(hashedPassword)
		
		// var passwordIsValid = bcryptjs.compareSync(req.body.password);
		// console.log(passwordIsValid)

		// UserRepository.create({
		// 	fullName: req.body.fullName,
		// 	email: req.body.email,
		// 	password: hashedPassword,
		// 	userRole: req.body.userRole,
		// 	confirm: false
		// },
		UserRepository.findByIdAndUpdate({ _id: req.params._id}, {password: hashedPassword, fullName: req.body.fullName, email: req.body.email},{ new: true },(err, users) => {
			console.log(err);
			if (err) return res.status(500).send("Error on the server.");
			res.json(users);
		}
		);
	}

	public deleteUser(req: RequestPost<{ _id: string }>, res: Response) {
		UserRepository.remove({ _id: req.params._id }, err => {
		if (err) return res.status(500).send("Error on the server.");
		console.log(err);
		res.json({ message: "Successfully deleted contact!" });
		});
	}

}
