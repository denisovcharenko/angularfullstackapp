import { Response } from "express";
import { RequestModel } from "../middlewares/authMiddleware";
import { MagazineRepository } from "../repositories/magazineRepository";
import { BookRepository } from "../repositories/bookRepository";

export class MagazineController {
  public searchMagazineByTitle(req: RequestModel<{}>, res: Response) {
    console.log(req.body);
    let reg = new RegExp(".*" + req.body.title + ".*", "i");
    // let regg = new RegExp('.*'+req.body.authorId+'.*', 'i');
    MagazineRepository.aggregate([
      {
        $lookup: {
          from: "authors",
          localField: "authorId",
          foreignField: "_id",
          as: "authorId"
        }
      },
      { $match: { title: reg } },
      { $limit: 10 }
    ]).then(magazine => [res.json(magazine)]);
  }

  // public getMagazine(req: RequestModel<{}>, res: Response) {
  //   MagazineRepository.find({}, (err, magazine) => {
  //     if (err) return res.status(500).send("Error on the server.");
  //     res.json(magazine);
  //   });
  // }
  public getMagazine(req: RequestModel<{}>, res: Response) {
    // const pageSize = +req.query.pagesize;
    // const currentPage = +req.query.page
    // let count = +req.query.totalMagazines
    // let skip = pageSize * (currentPage - 1) || 0
    MagazineRepository.aggregate([
      {
        $lookup: {
          from: "authors",
          localField: "authorId",
          foreignField: "_id",
          as: "authorId"
        }
      }
      // { $skip: skip },
      // { $limit: pageSize }
    ]).then(magazine => [res.json(magazine)]);
  }

  public getMagazineById(req: RequestModel<{ id: string }>, res: Response) {
    console.log(req.params);
    MagazineRepository.findById(req.params.id, (err, magazine) => {
      console.log(err);
      console.log(magazine);
      if (err) return res.status(500).send("Error on the server.");
      res.json(magazine);
    });
  }

  public addMagazine(req: RequestModel<{}>, res: Response) {
    let newMagazine = new MagazineRepository(req.body);
    console.log(newMagazine);
    newMagazine.save((err, magazine) => {
      if (err) return res.status(500).send("Error on the server.");
      res.status(200).send({ magazine: magazine });
    });
  }

  public updateMagazine(req: RequestModel<{ id: string }>, res: Response) {
    console.log(req.params);
    MagazineRepository.findOneAndUpdate(
      { _id: req.params.id },
      req.body,
      { new: true },
      (err, contact) => {
        if (err) return res.status(500).send("Error on the server.");
        res.json(contact);
      }
    );
  }

  public deleteMagazine(req: RequestModel<{ id: string }>, res: Response) {
    console.log(req.params);
    BookRepository.update(
      { magazine: req.params.id },
      { $pull: { magazine: req.params.id } },
      { multi: true },
      (err, book) => {
        MagazineRepository.remove({ _id: req.params.id }, err => {
          if (err) return res.status(500).send("Error on the server.");
          res.json({ message: "Successfully deleted magazine!" });
        });
      }
    );
  }
}
