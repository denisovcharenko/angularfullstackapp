import { RequestPost } from "../middlewares/authMiddleware";
import { Response } from "express";
import * as bcryptjs from "bcryptjs";
import * as jsonwebtoken from "jsonwebtoken";
import { authConfig } from "../config";
import { UserRepository } from "../repositories/userRepository";
import { userRole } from "../../../shared/models/user-role.model";
export class AuthController {
  public register(
    req: RequestPost<{
      fullName: string;
      email: string;
      password: string;
      confirm: boolean;
    }>,
    res: Response
  ) {
    var hashedPassword = bcryptjs.hashSync(req.body.password, 8);

    UserRepository.create(
      {
        fullName: req.body.fullName,
        email: req.body.email,
        password: hashedPassword,
        userRole: userRole.guest,
        confirm: false
      },
      (err, user) => {
        if (err)
          return res
            .status(500)
            .send({ msg: "There was a problem registering the user." });

        if (!req.body.email || !req.body.password) {
          return res.status(400).json({ msg: "Please enter all fields" });
        }
        console.log(err);
        // create a token

        res.status(200).send({ auth: true, user: user });
      }
    );
  }

  public login(
    req: RequestPost<{ email: string; password: string }>,
    res: Response
  ) {
    UserRepository.findOne({ email: req.body.email }, (err, user) => {
      if (err) return res.status(500).send({ msg: "Error on the server." });
      if (!user) return res.status(400).send({ msg: "No user found." });
      var passwordIsValid = bcryptjs.compareSync(
        req.body.password,
        user.password
      );
      if (!passwordIsValid)
        return res.status(401).send({ auth: false, token: null });
      var token = jsonwebtoken.sign(
        {
          id: user._id,
          fullName: user.fullName,
          email: user.email,
          userRole: user.userRole,
          confirm: user.confirm
        },
        authConfig.secret,
        {
          expiresIn: 86400 // expires in 24 hours
        }
      );
      if (!req.body.email || !req.body.password) {
        res.status(400).send({ msg: "Please enter all fields" });
      }

      res.status(200).send({ auth: true, token: token, user: user });
    });
  }
}
