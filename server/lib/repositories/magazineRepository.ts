import * as mongoose from 'mongoose';
import { IMagazineModel } from '../../../shared/models/magazine.model';

const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;
interface  IMagazineEntity extends IMagazineModel, mongoose.Document{ }

export const MagazineSchema = new Schema({
    title: String,
	img: String,
	authorId: [ObjectId]
});

export const MagazineRepository = mongoose.model<IMagazineEntity>('Magazine', MagazineSchema)