import * as mongoose from 'mongoose';
import { IBookModel } from '../../../shared/models/book.model';

const Schema = mongoose.Schema;
const ObjectId = mongoose.SchemaTypes.ObjectId;
interface IBookEntity extends IBookModel, mongoose.Document { }
export const BookSchema = new Schema({
	id: String,
	title: String,
	img: String,
	authorId: [ObjectId],
	magazine: [ObjectId],
	length: Number
});
export const BookRepository = mongoose.model<IBookEntity>('Book', BookSchema);