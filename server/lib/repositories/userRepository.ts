import * as mongoose from "mongoose";
import { IUserModel } from "../../../shared/models/user.model";

const Schema = mongoose.Schema;
interface IUserEntity extends IUserModel, mongoose.Document { }

export const UserSchema = new Schema({
  id: String,
  email: String,
  fullName: String,
  password: String,
  userRole: Number,
  confirm: Boolean
});
export const UserRepository = mongoose.model<IUserEntity>("User", UserSchema);
