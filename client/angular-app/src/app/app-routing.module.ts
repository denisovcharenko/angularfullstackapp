import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/auth/login/login.component';
import { RegistrationComponent } from './components/auth/registration/registration.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { GuestComponent } from './components/guest/guest.component';
import { HomePageComponent } from './components/user/home-page/home-page.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [
  {
    path: 'registration',
    component: HomePageComponent,
    children: [{ path: '', component: RegistrationComponent }]
  },
  {
    path: 'login',
    component: HomePageComponent,
    children: [{ path: '', component: LoginComponent }]
  },
  {
    path: 'user-profile',
    component: UserProfileComponent,
    canActivate: [AuthGuard],
    data: { userRole: [2], confirm: false }
  },
  {
    path: 'admin',
    loadChildren: './components/admin/admin.module#AdminModule',
    canActivate: [AuthGuard],
    data: { userRole: [1] }
  },
  {
    path: 'guest-profile',
    component: GuestComponent,
    canActivate: [AuthGuard],
    data: { userRole: [0], confirm: true }
  },
  {
    path: 'error-page',
    component: ErrorPageComponent,
    canActivate: [AuthGuard],
    data: { userRole: [0, 1, 2], confirm: false }
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {}
