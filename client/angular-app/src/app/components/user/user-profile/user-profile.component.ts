import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IUserModel } from '@models/user.model';
import { LocalStorageService } from '@services/localStorage.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  user: IUserModel[];
  userDetails = this.userService.getUserPayload();

  constructor(
    private router: Router,
    private localStorageService: LocalStorageService,
    private userService: UserService
  ) {
    this.user = [
      {
        _id: '',
        email: '',
        fullName: '',
        password: '',
        userRole: null,
        userId: '',
        confirm: false
      }
    ];
  }

  ngOnInit() {
    this.getUserById();
  }

  getUserById() {
    const userPayloadId = this.userService.getUserPayload().id;
    this.userService.getById(userPayloadId).subscribe(user => {
      this.user = Array.of(user);
    });
  }

  onLogout() {
    this.localStorageService.deleteItem('token');
    this.router.navigate(['/login']);
  }
}
