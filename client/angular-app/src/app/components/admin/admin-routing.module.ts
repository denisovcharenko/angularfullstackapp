import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminComponent } from './admin.component';

const routes: Routes = [
  {
    path: '', component: AdminComponent, children: [
      {
        path: '', component:  AdminDashboardComponent
      },
      {
        path: 'users', loadChildren: './users/users.module#UsersModule'
      },
      {
        path: 'authors', loadChildren: './authors/author.module#AuthorModule'
      },
      {
        path: 'magazines', loadChildren: './magazines/magazine.module#MagazineModule'
      },
      {
        path: 'books', loadChildren: './books/book.module#BookModule'
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
