import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IUserModel } from '@models/user.model';
import { LocalStorageService } from '@services/localStorage.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.sass']
})
export class ListUsersComponent implements OnInit {
  users: IUserModel[];
  token = this.localStorageService.getItemValue('token');

  constructor(
    private router: Router,
    private localStorageService: LocalStorageService,
    private userService: UserService
  ) {
    this.users = [
      {
        _id: '',
        email: '',
        fullName: '',
        password: '',
        userRole: null,
        userId: '',
        confirm: false
      }
    ];
  }

  ngOnInit() {
    this.listOfUsers();
  }

  deleteUser(id: string) {
    this.userService.deleteUser(id).subscribe(() => {
      this.listOfUsers();
    });
  }

  editUser(id: string, i: number) {
    const user = this.users[i];
    this.userService.editUser(user, id).subscribe(response => {
      this.users = response;
    });
  }

  changeRoleUser(event, id: string, i: number) {
    const user = this.users[i];
    user.userRole = +event.target.value;
    this.userService.changeRole(user, id);
  }

  confirm(id: string, i: number) {
    const user = this.users[i];
    if (user.confirm === false) {
      user.confirm = true;
    } else {
      user.confirm = false;
    }
    this.userService.changeConfirm(user, id);
  }

  listOfUsers() {
    this.userService.listUsers().subscribe(user => {
      this.users = user;
    });
  }

  onLogout() {
    this.localStorageService.deleteItem('token');
    this.router.navigate(['/login']);
  }
}
