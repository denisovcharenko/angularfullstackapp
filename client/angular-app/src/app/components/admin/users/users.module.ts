import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDialogModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSelectModule
} from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

import { AddUserComponent } from './add-user/add-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { UsersRoutingModule } from './users-routing.module';

const COMPONENTS = [AddUserComponent, ListUsersComponent, EditUserComponent];

const MODULES = [
  UsersRoutingModule,
  CommonModule,
  ReactiveFormsModule,
  HttpClientModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  NgSelectModule,
  FormsModule,
  MatDialogModule,
  Ng2SearchPipeModule,
  NgxPaginationModule,
  MatPaginatorModule,
  ReactiveFormsModule
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES]
})
export class UsersModule {}
export class MaterialModule {}
