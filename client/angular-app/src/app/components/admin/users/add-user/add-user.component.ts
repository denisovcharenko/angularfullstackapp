import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  addUserForm: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private userService: UserService,
    private router: Router,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
  }

  get f() {
    return this.addUserForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.addUserForm.invalid) {
      return;
    }
    this.userService.addUser(this.addUserForm.value).subscribe(
      () => {
        this.router.navigate(['admin/users']);
      },
      () => {
        this.loading = false;
      }
    );
  }

  private initForm() {
    this.addUserForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: [''],
      password: ['', [Validators.required, Validators.minLength(4)]],
      userRole: [''],
      confirm: ['']
    });
  }
}
