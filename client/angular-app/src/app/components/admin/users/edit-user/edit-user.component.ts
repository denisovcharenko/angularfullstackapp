import { first } from 'rxjs/operators';

import { Component, OnInit } from '@angular/core';
import { EmailValidator, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {
  editForm: FormGroup;
  submitted = false;
  loading = false;
  idPage = '';
  newPassword = '';

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.getPageId();
    this.getUserById();
    this.initForm();
  }

  get f() {
    return this.editForm.controls;
  }

  getUserById() {
    this.userService.getById(this.idPage).subscribe(data => {
      this.editForm.setValue(data);
    });
  }

  getPageId() {
    this.route.params.subscribe(params => {
      this.idPage = params.userId;
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.editForm.invalid) {
      return;
    }
    this.loading = true;
    const formData: any = {
      email: this.editForm.value.email,
      fullName: this.editForm.value.fullName,
      password: this.editForm.value.password
    };
    this.userService
      .editUser(formData, this.idPage)
      .pipe(first())
      .subscribe(
        () => {
          this.router.navigate(['admin/users']);
        },
        () => {
          this.loading = false;
        }
      );
  }

  private initForm() {
    this.editForm = this.formBuilder.group({
      _id: [''],
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email, EmailValidator]],
      password: '',
      __v: [''],
      confirm: ['']
    });
  }
}
