import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddAuthorComponent } from './add-author/add-author.component';
import { EditAuthorComponent } from './edit-author/edit-author.component';
import { ListAuthorsComponent } from './list-authors/list-authors.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ListAuthorsComponent },
      { path: 'edit-author/:authorId', component: EditAuthorComponent },
      { path: 'add-author', component: AddAuthorComponent }
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorRoutingModule {}
