import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { IAuthorModel } from '@models/author.model';
import { AuthorService } from '@services/author.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-edit-author',
  templateUrl: './edit-author.component.html',
  styleUrls: ['./edit-author.component.sass']
})
export class EditAuthorComponent implements OnInit {
  success = 'You successfully edited this author';
  errorMsg = 'Error, please fill in all fields';
  result = '';
  submitted = false;
  loading = false;
  editAuthorForm: FormGroup;
  authors: IAuthorModel[] = [];
  author: IAuthorModel;
  idPage = '';

  constructor(
    private route: ActivatedRoute,
    private authorService: AuthorService,
    private dialog: MatDialog,
    private router: Router
  ) {
   this.initForm();
  }

  get f() {
    return this.editAuthorForm.controls;
  }

  ngOnInit() {
    this.getPageId();
    this.getUserById();
  }

  getPageId() {
    this.route.params.subscribe(params => {
      this.idPage = params.authorId;
    });
  }

  getUserById() {
    this.authorService.getAuthorById(this.idPage).subscribe(data => {
      this.author = data;
      this.editAuthorForm.setValue(data);
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.editAuthorForm.invalid) {
      return;
    }
    this.loading = true;
    const formData: any = {
      fullName: this.editAuthorForm.value.fullName
    };

    this.authorService
      .editAuthor({ author: formData, id: this.idPage })
      .subscribe(
        () => {
          this.dialog.open(ModalComponent, {
            data: {
              result: this.success
            }
          });
          this.router.navigate(['/authors']);
        },
        () => {
          this.dialog.open(ModalComponent, {
            data: {
              result: this.errorMsg
            }
          });
        }
      );
  }

  private initForm() {
    this.editAuthorForm = new FormGroup({
      fullName: new FormControl(),
      _id: new FormControl(),
      __v: new FormControl()
    });
  }
}
