import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { AuthorService } from '@services/author.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-add-author',
  templateUrl: './add-author.component.html',
  styleUrls: ['./add-author.component.sass']
})
export class AddAuthorComponent implements OnInit {
  success = 'You successfully added author';
  errorMsg = 'Something error was happened';
  result = '';
  addAuthorForm: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private authorService: AuthorService,
    private formBuilder: FormBuilder,
    private dialog: MatDialog) { }

  ngOnInit() {
    this.initForm();
  }

  get f() {
    return this.addAuthorForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.addAuthorForm.invalid) {
      return;
    }
    this.loading = true;
    this.authorService.addAuthor(this.addAuthorForm.value)
    .subscribe(
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.success
          }
        });
      },
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.errorMsg
          }
        });
        this.loading = false;
      }
    );
  }
  private initForm() {
    this.addAuthorForm = this.formBuilder.group({
      fullName: ['', Validators.required]
    });
  }
}
