import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDialogModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSelectModule
} from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

import { AddAuthorComponent } from './add-author/add-author.component';
import { AuthorRoutingModule } from './author-routing.module';
import { EditAuthorComponent } from './edit-author/edit-author.component';
import { ListAuthorsComponent } from './list-authors/list-authors.component';

const COMPONENTS = [
  AddAuthorComponent,
  ListAuthorsComponent,
  EditAuthorComponent
];

const MODULES = [
  CommonModule,
  AuthorRoutingModule,
  ReactiveFormsModule,
  HttpClientModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  NgSelectModule,
  FormsModule,
  MatDialogModule,
  Ng2SearchPipeModule,
  NgxPaginationModule,
  MatPaginatorModule
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES]
})
export class AuthorModule {}
