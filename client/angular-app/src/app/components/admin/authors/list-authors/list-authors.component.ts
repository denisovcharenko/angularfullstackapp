import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { IAuthorModel } from '@models/author.model';
import { AuthorService } from '@services/author.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-list-authors',
  templateUrl: './list-authors.component.html',
  styleUrls: ['./list-authors.component.sass']
})
export class ListAuthorsComponent implements OnInit {
  success = 'You successfully deleted this author';
  errorMsg = 'Something error was happened';
  result = '';
  authors: IAuthorModel[] = [];

  constructor(private authorService: AuthorService, private dialog: MatDialog) {
    this.authors = [
      {
        _id: '',
        fullName: ''
      }
    ];
  }

  ngOnInit() {
    this.listAuthors();
  }

  listAuthors() {
    this.authorService.getAuthors().subscribe((response: IAuthorModel[]) => {
      this.authors = response;
    });
  }

  deleteAuthor(id: string) {
    this.authorService.deleteAuthor(id).subscribe(() => {
      this.dialog.open(ModalComponent, {
        data: {
          result: this.success
        }
      });
      this.listAuthors();
    });
  }
}
