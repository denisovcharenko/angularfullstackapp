import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';
import { HeaderComponent } from 'src/app/containers/header/header.component';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDialogModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSelectModule
} from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';

@NgModule({
  declarations: [
    AdminComponent,
    AdminDashboardComponent,
    HeaderComponent,

  ],
  imports: [
    AdminRoutingModule,
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    NgSelectModule,
    FormsModule,
    MatDialogModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    MatPaginatorModule,
    ReactiveFormsModule,
  ],
})
export class AdminModule { }
export class MaterialModule {}
