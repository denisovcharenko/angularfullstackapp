import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { IAuthorModel } from '@models/author.model';
import { AuthorService } from '@services/author.service';
import { MagazineService } from '@services/magazine.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-add-magazine',
  templateUrl: './add-magazine.component.html',
  styleUrls: ['./add-magazine.component.sass']
})
export class AddMagazineComponent implements OnInit {
  success = 'You successfully added magazine';
  errorMsg = 'Something error was happened';
  result = '';
  submitted = false;
  loading = false;
  selectedFile = null;
  selectedAuthor = [];
  addMagazineForm = new FormGroup({
    title: new FormControl(),
    img: new FormControl(),
    authorId: new FormControl()
  });
  authors: IAuthorModel[];

  constructor(
    private magazineService: MagazineService,
    private authorService: AuthorService,
    private dialog: MatDialog
  ) {
    this.authors = [
      {
        _id: '',
        fullName: ''
      }
    ];
  }

  ngOnInit() {
    this.listOfAuthors();
  }

  get f() {
    return this.addMagazineForm.controls;
  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.selectedFile = reader.result;
    };
    reader.readAsDataURL(this.selectedFile);
  }

  onAuthorSelected(event) {
    this.selectedAuthor = event.map(item => {
      return item._id;
    });
  }

  listOfAuthors() {
    this.authorService.getAuthors().subscribe(author => {
      this.authors = author;
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.addMagazineForm.invalid) {
      return;
    }
    this.loading = true;
    const formdata: any = {
      title: this.addMagazineForm.value.title,
      img: this.selectedFile,
      authorId: this.selectedAuthor
    };
    this.magazineService.addMagazine(formdata).subscribe(
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.success
          }
        });
      },
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.errorMsg
          }
        });
        this.loading = false;
      }
    );
  }
}
