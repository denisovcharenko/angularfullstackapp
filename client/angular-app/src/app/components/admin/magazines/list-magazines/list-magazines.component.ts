import { Component, OnInit } from '@angular/core';
import { MatDialog, PageEvent } from '@angular/material';
import { IMagazineModel } from '@models/magazine.model';
import { MagazineService } from '@services/magazine.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-list-magazines',
  templateUrl: './list-magazines.component.html',
  styleUrls: ['./list-magazines.component.sass']
})
export class ListMagazinesComponent implements OnInit {
  totalMagazines = 6;
  magazinesPerPage = 2;
  pageSizeOption = [1, 2, 5, 10];
  currentPage = 1;
  success = 'You successfully deleted this book';
  errorMsg = 'Something error was happened';
  result = '';
  magazines: IMagazineModel[] = [];

  constructor(
    private magazineService: MagazineService,
    private dialog: MatDialog
  ) {
    this.magazines = [
      {
        _id: '',
        title: '',
        img: '',
        authorId: []
      }
    ];
  }

  onChangedPage(pageData: PageEvent) {
    this.currentPage = pageData.pageIndex + 1;
    this.magazinesPerPage = pageData.pageSize;
    this.magazineService
      .getMagazines(this.magazinesPerPage, this.currentPage)
      .subscribe((response: IMagazineModel[]) => {
        this.magazines = response;
      });
  }

  ngOnInit() {
    this.listMagazines();
  }

  listMagazines() {
    this.magazineService
      .getMagazines(this.magazinesPerPage, this.currentPage)
      .subscribe((response: IMagazineModel[]) => {
        this.magazines = response;
      });
  }

  onSearchMagazine(event) {
    this.magazineService.searchMagazine(event.target.value).subscribe(res => {
      this.magazines = res;
    });
  }

  deleteMagazine(id: string) {
    this.magazineService.deleteMagazine(id).subscribe(() => {
      this.dialog.open(ModalComponent, {
        data: {
          result: this.success
        }
      });
      this.listMagazines();
    });
  }
}
