import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDialogModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSelectModule
} from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

import { AddMagazineComponent } from './add-magazine/add-magazine.component';
import { EditMagazineComponent } from './edit-magazine/edit-magazine.component';
import { ListMagazinesComponent } from './list-magazines/list-magazines.component';
import { MagazineRoutingModule } from './magazine-routing.module';

const COMPONENTS = [
  AddMagazineComponent,
  ListMagazinesComponent,
  EditMagazineComponent
];

const MODULES = [
  CommonModule,
  MagazineRoutingModule,
  ReactiveFormsModule,
  HttpClientModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  NgSelectModule,
  FormsModule,
  MatDialogModule,
  Ng2SearchPipeModule,
  NgxPaginationModule,
  MatPaginatorModule
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES]
})
export class MagazineModule {}
