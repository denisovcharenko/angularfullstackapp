import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { IAuthorModel } from '@models/author.model';
import { IMagazineModel } from '@models/magazine.model';
import { AuthorService } from '@services/author.service';
import { MagazineService } from '@services/magazine.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-edit-magazine',
  templateUrl: './edit-magazine.component.html',
  styleUrls: ['./edit-magazine.component.sass']
})
export class EditMagazineComponent implements OnInit {
  success = 'You successfully edited this magazine';
  errorMsg = 'Error, please fill in all fields';
  result = '';
  submitted = false;
  loading = false;
  editMagazineForm: FormGroup;
  selectedFile = null;
  selectedAuthor = [];
  magazines: IMagazineModel[] = [];
  authors: IAuthorModel[] = [];
  idPage = '';

  constructor(
    private magazineService: MagazineService,
    private route: ActivatedRoute,
    private authorService: AuthorService,
    private dialog: MatDialog,
    private router: Router
  ) {
    this.editMagazineForm = new FormGroup({
      title: new FormControl(),
      img: new FormControl(),
      authorId: new FormControl(),
      _id: new FormControl(),
      __v: new FormControl()
    });
  }

  ngOnInit() {
    this.listOfAuthors();
    this.getPageId();
    this.getMagazineById();
  }

  get f() {
    return this.editMagazineForm.controls;
  }

  getMagazineById() {
    this.magazineService.getMagazineById(this.idPage).subscribe(data => {
      this.magazines = data;
      this.editMagazineForm.setValue(data);
    });
  }

  getPageId() {
    this.route.params.subscribe(params => {
      this.idPage = params.magazineId;
    });
  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.selectedFile = reader.result;
    };
    reader.readAsDataURL(this.selectedFile);
  }

  onAuthorSelected(event) {
    this.selectedAuthor = event.map(item => {
      return item._id;
    });
  }

  listOfAuthors() {
    this.authorService.getAuthors().subscribe(author => {
      this.authors = author;
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.editMagazineForm.invalid) {
      return;
    }
    this.loading = true;
    const formData: any = {
      title: this.editMagazineForm.value.title,
      img: this.selectedFile || this.editMagazineForm.value.img,
      authorId: this.editMagazineForm.value.authorId
    };

    this.magazineService.editMagazine(formData, this.idPage).subscribe(
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.success
          }
        });
        this.router.navigate(['magazines']);
      },
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.errorMsg
          }
        });
      }
    );
  }
}
