import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddMagazineComponent } from './add-magazine/add-magazine.component';
import { EditMagazineComponent } from './edit-magazine/edit-magazine.component';
import { ListMagazinesComponent } from './list-magazines/list-magazines.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ListMagazinesComponent },
      { path: 'edit-magazine/:magazineId', component: EditMagazineComponent },
      { path: 'add-magazine', component: AddMagazineComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MagazineRoutingModule {}
