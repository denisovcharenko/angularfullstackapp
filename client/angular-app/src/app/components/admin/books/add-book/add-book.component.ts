import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { IAuthorModel } from '@models/author.model';
import { IMagazineModel } from '@models/magazine.model';
import { AuthorService } from '@services/author.service';
import { BookService } from '@services/book.service';
import { MagazineService } from '@services/magazine.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.scss']
})
export class AddBookComponent implements OnInit {
  success = 'You successfully added this book';
  errorMsg = 'Error, please fill in all fields';
  result = '';
  magazinesPerPage = 2;
  currentPage = 1;
  closedResult: string;
  submitted = false;
  loading = false;
  selectedFile = null;
  selectedAuthor = [];
  selectedMagazine = [];
  addBookForm = new FormGroup({
    title: new FormControl(),
    img: new FormControl(),
    authorId: new FormControl(),
    magazine: new FormControl()
  });
  authors: IAuthorModel[] = [];
  magazines: IMagazineModel[] = [];

  constructor(
    private bookService: BookService,
    private authorService: AuthorService,
    private magazineService: MagazineService,
    private dialog: MatDialog,
    private router: Router
  ) {
    this.authors = [
      {
        _id: '',
        fullName: ''
      }
    ];
    this.magazines = [
      {
        _id: '',
        title: '',
        img: '',
        authorId: []
      }
    ];
  }

  ngOnInit() {
    this.listOfAuthors();
    this.listOfMagazines();
  }

  get f() {
    return this.addBookForm.controls;
  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.selectedFile = reader.result;
    };
    reader.readAsDataURL(this.selectedFile);
  }

  onAuthorSelected(event) {
    this.selectedAuthor = event.map(item => {
      return item._id;
    });
  }

  onMagazineSelected(event) {
    this.selectedMagazine = event.map(item => {
      return item._id;
    });
  }

  listOfAuthors() {
    this.authorService.getAuthors().subscribe(author => {
      this.authors = author;
    });
  }

  listOfMagazines() {
    this.magazineService
      .getMagazines(this.magazinesPerPage, this.currentPage)
      .subscribe(magazine => {
        this.magazines = magazine;
      });
  }

  onSubmit() {
    this.submitted = true;
    if (this.addBookForm.invalid) {
      return;
    }
    this.loading = true;
    const formData: any = {
      title: this.addBookForm.value.title,
      img: this.selectedFile,
      authorId: this.selectedAuthor,
      magazine: this.selectedMagazine
    };
    this.bookService.addBook(formData).subscribe(
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.success
          }
        });
        setTimeout(() => {
          this.router.navigate(['/books']);
        }, 3000);
      },
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.errorMsg
          }
        });
      }
    );
  }
}
