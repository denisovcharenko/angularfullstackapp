import { Component, OnInit } from '@angular/core';
import { MatDialog, PageEvent } from '@angular/material';
import { IAuthorModel } from '@models/author.model';
import { IBookModel } from '@models/book.model';
import { IMagazineModel } from '@models/magazine.model';
import { BookService } from '@services/book.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-list-books',
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.scss']
})
export class ListBooksComponent implements OnInit {
  totalBooks = 6;
  booksPerPage = 2;
  pageSizeOption = [1, 2, 5, 10];
  currentPage = 1;
  success = 'You successfully deleted this book';
  errorMsg = 'Something error was happened';
  result = '';
  magazineItems: IMagazineModel[] = [];
  authorItems: IAuthorModel[] = [];
  books: IBookModel[] = [];

  constructor(private bookService: BookService, private dialog: MatDialog) {
    this.books = [
      {
        _id: '',
        title: '',
        img: '',
        fullName: '',
        authorId: [],
        magazine: []
      }
    ];
  }

  ngOnInit() {
    this.listBooks();
  }

  onChangedPage(pageData: PageEvent) {
    this.currentPage = pageData.pageIndex + 1;
    this.booksPerPage = pageData.pageSize;
    this.bookService
      .getBooks(this.booksPerPage, this.currentPage)
      .subscribe((response: IBookModel[]) => {
        this.books = response;
      });
  }

  deleteBook(id: string) {
    this.bookService.deleteBook(id).subscribe(() => {
      this.dialog.open(ModalComponent, {
        data: {
          result: this.success
        }
      });
      this.listBooks();
    });
  }

  listBooks() {
    this.bookService
      .getBooks(this.booksPerPage, this.currentPage)
      .subscribe((response: IBookModel[]) => {
        this.books = response;
      });
  }

  onSearchBook(event) {
    this.bookService.searchBook(event.target.value).subscribe(response => {
      this.books = response;
    });
  }
}
