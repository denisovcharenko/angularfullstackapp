import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDialogModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSelectModule
} from '@angular/material';
import { NgSelectModule } from '@ng-select/ng-select';

import { AddBookComponent } from './add-book/add-book.component';
import { BookRoutingModule } from './book-routing.module';
import { EditBookComponent } from './edit-book/edit-book.component';
import { ListBooksComponent } from './list-books/list-books.component';

const COMPONENTS = [ListBooksComponent, EditBookComponent, AddBookComponent];

const MODULES = [
  CommonModule,
  BookRoutingModule,
  ReactiveFormsModule,
  HttpClientModule,
  MatSelectModule,
  MatFormFieldModule,
  MatInputModule,
  NgSelectModule,
  FormsModule,
  MatDialogModule,
  Ng2SearchPipeModule,
  NgxPaginationModule,
  MatPaginatorModule
];

@NgModule({
  declarations: [...COMPONENTS],
  imports: [...MODULES]
})
export class BookModule {}
