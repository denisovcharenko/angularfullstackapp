import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddBookComponent } from './add-book/add-book.component';
import { EditBookComponent } from './edit-book/edit-book.component';
import { ListBooksComponent } from './list-books/list-books.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: '', component: ListBooksComponent },
      { path: 'add-book', component: AddBookComponent },
      { path: 'edit-book/:bookId', component: EditBookComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookRoutingModule {}
