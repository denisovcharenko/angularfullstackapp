import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { IAuthorModel } from '@models/author.model';
import { IBookModel } from '@models/book.model';
import { IMagazineModel } from '@models/magazine.model';
import { AuthorService } from '@services/author.service';
import { BookService } from '@services/book.service';
import { MagazineService } from '@services/magazine.service';

import { ModalComponent } from '../../modal/modal.component';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.sass']
})
export class EditBookComponent implements OnInit {
  success = 'You successfully edited this book';
  errorMsg = 'Error, please fill in all fields';
  result = '';
  closedResult: string;
  magazinesPerPage = 2;
  currentPage = 1;
  editBookForm: FormGroup;
  submitted = false;
  loading = false;
  selectedFile = null;
  selectedAuthor = [];
  selectedMagazine = [];
  idPage = '';
  IdAuthor = '';
  IdMagazine = '';
  authors: IAuthorModel[] = [];
  magazines: IMagazineModel[] = [];
  books: IBookModel[] = [];

  constructor(
    private router: Router,
    private bookService: BookService,
    private route: ActivatedRoute,
    private authorService: AuthorService,
    private magazineService: MagazineService,
    private dialog: MatDialog
  ) {
    this.editBookForm = new FormGroup({
      title: new FormControl(),
      img: new FormControl(),
      authorId: new FormControl(),
      magazine: new FormControl(),
      _id: new FormControl(),
      __v: new FormControl()
    });
  }

  ngOnInit() {
    this.listOfAuthors();
    this.listOfMagazines();
    this.getPageId();
    this.getBookById();
  }

  getBookById() {
    this.bookService.getBookById(this.idPage).subscribe(data => {
      this.books = data;
      this.editBookForm.setValue(data);
    });
  }

  getPageId() {
    this.route.params.subscribe(params => {
      this.idPage = params.bookId;
    });
  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.selectedFile = reader.result;
    };
    reader.readAsDataURL(this.selectedFile);
  }

  onAuthorSelected(event) {
    this.selectedAuthor = event.map(item => {
      return item._id;
    });
  }

  onMagazineSelected(event) {
    this.selectedMagazine = event.map(item => {
      return item._id;
    });
  }

  listOfAuthors() {
    this.authorService.getAuthors().subscribe(author => {
      this.authors = author;
    });
  }

  listOfMagazines() {
    this.magazineService
      .getMagazines(this.magazinesPerPage, this.currentPage)
      .subscribe(magazine => {
        this.magazines = magazine;
      });
  }

  onSubmit() {
    this.submitted = true;

    if (this.editBookForm.invalid) {
      return;
    }
    this.loading = true;
    const formData: any = {
      title: this.editBookForm.value.title,
      img: this.selectedFile || this.editBookForm.value.img,
      authorId: this.editBookForm.value.authorId,
      magazine: this.editBookForm.value.magazine
    };

    this.bookService.editBook(formData, this.idPage).subscribe(
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.success
          }
        });
        this.router.navigate(['/list-books']);
      },
      () => {
        this.dialog.open(ModalComponent, {
          data: {
            result: this.errorMsg
          }
        });
      }
    );
  }
}
