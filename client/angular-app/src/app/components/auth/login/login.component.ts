import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@services/auth.service';
import { LocalStorageService } from '@services/localStorage.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private localStorageService: LocalStorageService,
    private formBulder: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.initForm();

    if (
      this.authService.isLoggedIn() &&
      this.userService.getUserPayload().userRole === 1
    ) {
      this.router.navigateByUrl('/admin');
    }
    if (
      this.authService.isLoggedIn() &&
      this.userService.getUserPayload().userRole === 2
    ) {
      this.router.navigateByUrl('/user-profile');
    }
    if (
      this.authService.isLoggedIn() &&
      this.userService.getUserPayload().userRole === 0
    ) {
      this.router.navigateByUrl('/guest-profile');
    }
  }

  get f() {
    return this.loginForm.controls;
  }

  login(): void {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.login(this.loginForm.value).subscribe(res => {
      this.localStorageService.setItem('token', JSON.stringify(res));
      if (
        this.authService.isLoggedIn() &&
        this.userService.getUserPayload().userRole === 1
      ) {
        this.router.navigateByUrl('/admin');
      }
      if (
        this.authService.isLoggedIn() &&
        this.userService.getUserPayload().userRole === 2
      ) {
        this.router.navigateByUrl('/user-profile');
      }
      if (
        this.authService.isLoggedIn() &&
        this.userService.getUserPayload().userRole === 0
      ) {
        this.router.navigateByUrl('/guest-profile');
      }
    });
  }

  private initForm() {
    this.loginForm = this.formBulder.group({
      email: [
        '',
        Validators.required,
        Validators.pattern(
          /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/
        )
      ],
      password: ['', Validators.required]
    });
  }
}
