import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '@services/auth.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  loading = false;

  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuider: FormBuilder
  ) {}

  ngOnInit() {
    this.initForm();
    if (this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/user-profile');
    }
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }
    this.loading = true;
    this.authService.registrationAuth(this.registerForm.value).subscribe(
      () => {
        this.router.navigate(['/login']);
      },
      () => {
        this.loading = false;
      }
    );
  }

  private initForm() {
    this.registerForm = this.formBuider.group({
      fullName: ['', Validators.required],
      email: [''],
      password: ['', [Validators.required, Validators.minLength(4)]]
    });
  }
}
