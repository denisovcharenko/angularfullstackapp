import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IUserModel } from '@models/user.model';
import { AuthService } from '@services/auth.service';
import { LocalStorageService } from '@services/localStorage.service';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.component.html',
  styleUrls: ['./guest.component.scss']
})
export class GuestComponent {
  users: IUserModel[];

  constructor(
    public authService: AuthService,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {}

  onLogout() {
    this.localStorageService.deleteItem('token');
    this.router.navigate(['/login']);
  }
}
