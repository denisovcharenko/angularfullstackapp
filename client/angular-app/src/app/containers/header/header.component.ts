import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { IUserModel } from '@models/user.model';
import { AuthService } from '@services/auth.service';
import { LocalStorageService } from '@services/localStorage.service';

interface DropDown {
  mainTitle: string;
  mainUrl: string;
  links: Array<{ title: string; url: string }>;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  user: IUserModel[];

  dropDown: DropDown[] = [
    {
      mainTitle: 'Magazine',
      mainUrl: 'magazines',
      links: [
        {
          title: 'add magazine',
          url: 'magazines/addmagazine'
        }
      ]
    },
    {
      mainTitle: 'Authors',
      mainUrl: 'authors',
      links: [
        {
          title: 'add author',
          url: 'authors/addauthor'
        }
      ]
    },
    {
      mainTitle: 'Books',
      mainUrl: 'books',
      links: [
        {
          title: 'add book',
          url: 'books/addbook'
        }
      ]
    },
    {
      mainTitle: 'Users',
      mainUrl: 'users',
      links: [
        {
          title: 'add user',
          url: 'users/adduser'
        }
      ]
    }
  ];
  constructor(
    private router: Router,
    public localStorageService: LocalStorageService,
    public authService: AuthService
  ) {}

  onLogout() {
    this.localStorageService.deleteItem('token');
    this.router.navigate(['/login']);
  }
}
