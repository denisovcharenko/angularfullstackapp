import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { NgxPaginationModule } from 'ngx-pagination';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {
    MatDialogModule, MatFormFieldModule, MatInputModule, MatPaginatorModule, MatSelectModule
} from '@angular/material';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ModalComponent } from './components/admin/modal/modal.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegistrationComponent } from './components/auth/registration/registration.component';
import { ErrorPageComponent } from './components/error-page/error-page.component';
import { GuestComponent } from './components/guest/guest.component';
import { HomePageComponent } from './components/user/home-page/home-page.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';
import { EmailValidatorDirective } from './shared/directive/emailValidation.directive';
import { AuthGuard } from './shared/guards/auth.guard';
import { AuthInterceptor } from './shared/interceptor/auth.interceptor';
import { AuthService } from './shared/services/auth.service';

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    HomePageComponent,
    LoginComponent,
    UserProfileComponent,
    GuestComponent,
    ModalComponent,
    EmailValidatorDirective,
    ErrorPageComponent
  ],
  imports: [
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    NgSelectModule,
    FormsModule,
    MatDialogModule,
    Ng2SearchPipeModule,
    NgxPaginationModule,
    MatPaginatorModule,
    BrowserModule,
    BrowserAnimationsModule
  ],
  exports: [
    MatFormFieldModule,
    MatSelectModule,
    MatDialogModule,
    EmailValidatorDirective
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    AuthService,
    AuthGuard
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalComponent]
})
export class AppModule {}
export class MaterialModule {}
