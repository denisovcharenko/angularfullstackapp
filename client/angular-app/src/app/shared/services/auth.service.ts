import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { IAuthModel } from '@models/auth.model';

import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  selectedUser: IAuthModel = {
    fullName: '',
    email: '',
    password: ''
  };

  private baseUrl = environment.apiBaseUrl + '/auth';

  constructor(private http: HttpClient, private userService: UserService) {}

  registrationAuth(auth: IAuthModel): Observable<IAuthModel> {
    return this.http.post<IAuthModel>(this.baseUrl + '/register', auth);
  }

  login(authCredintials: any): Observable<IAuthModel> {
    return this.http.post<IAuthModel>(this.baseUrl + '/login', authCredintials);
  }

  isLoggedIn(): boolean {
    const userPayload = this.userService.getUserPayload();
    if (userPayload) {
      return userPayload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }
}
