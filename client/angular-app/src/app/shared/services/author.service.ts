import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { IAuthorModel } from '@models/author.model';

@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  author: IAuthorModel = {
    _id: '',
    fullName: ''
  };

  private baseUrl = environment.apiBaseUrl + '/author';

  constructor(private http: HttpClient) {}

  addAuthor(author: IAuthorModel): Observable<IAuthorModel> {
    return this.http.post<IAuthorModel>(this.baseUrl, author);
  }

  getAuthors(): Observable<IAuthorModel[]> {
    return this.http.get<IAuthorModel[]>(this.baseUrl);
  }

  getAuthorById(id: any): Observable<IAuthorModel> {
    return this.http.get<IAuthorModel>(this.baseUrl + id);
  }

  deleteAuthor(id: string): Observable<IAuthorModel> {
    return this.http.delete<IAuthorModel>(this.baseUrl + id);
  }

  editAuthor({
    author,
    id
  }: {
    author: IAuthorModel;
    id: string;
  }): Observable<IAuthorModel> {
    const body: IAuthorModel = author;
    return this.http.put<IAuthorModel>(this.baseUrl + id, body);
  }
}
