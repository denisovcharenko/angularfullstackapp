import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { IMagazineModel } from '@models/magazine.model';

@Injectable({
  providedIn: 'root'
})
export class MagazineService {
  magazine: IMagazineModel = {
    _id: '',
    title: '',
    img: '',
    authorId: []
  };

  private baseUrl = environment.apiBaseUrl + '/magazine';

  constructor(private http: HttpClient) {}

  addMagazine(magazine: IMagazineModel): Observable<IMagazineModel> {
    return this.http.post<IMagazineModel>(this.baseUrl, magazine);
  }

  getMagazines(
    magazinesPerPage: number,
    currentPage: number
  ): Observable<IMagazineModel[]> {
    const queryParams = `?pagesize=${magazinesPerPage}&page=${currentPage}`;
    return this.http.get<IMagazineModel[]>(this.baseUrl + '/' + queryParams);
  }

  getMagazineById(id: string): Observable<IMagazineModel[]> {
    return this.http.get<IMagazineModel[]>(this.baseUrl + '/' + id);
  }

  searchMagazine(title: string): Observable<IMagazineModel[]> {
    const body = { title };
    return this.http.post<IMagazineModel[]>(this.baseUrl + '/search', body);
  }

  deleteMagazine(id: string): Observable<IMagazineModel[]> {
    return this.http.delete<IMagazineModel[]>(this.baseUrl + '/' + id);
  }

  editMagazine(
    magazine: IMagazineModel,
    id: string
  ): Observable<IMagazineModel> {
    return this.http.put<IMagazineModel>(this.baseUrl + '/' + id, magazine);
  }
}
