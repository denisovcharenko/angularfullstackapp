import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {
  setItem(title: string, item: any) {
    localStorage.setItem(title, item);
  }

  deleteItem(item: any) {
    localStorage.removeItem(item);
  }

  getItemValueString(key: string): string {
    return localStorage.getItem(key);
  }

  getItemValue<T>(key: string): T {
    const value: string = localStorage.getItem(key);
    return JSON.parse(value) as T;
  }
}
