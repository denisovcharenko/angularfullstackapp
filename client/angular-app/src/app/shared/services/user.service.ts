import * as jwt_decode from 'jwt-decode';
import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { IUserModel } from '@models/user.model';

import { LocalStorageService } from './localStorage.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  user: IUserModel = {
    _id: '',
    email: '',
    fullName: '',
    password: '',
    userRole: null,
    userId: '',
    confirm: false
  };

  private baseUrl = environment.apiBaseUrl + '/auth/profile';

  constructor(
    private http: HttpClient,
    private localStorage: LocalStorageService
  ) {}

  listUsers(): Observable<IUserModel[]> {
    return this.http.get<IUserModel[]>(this.baseUrl);
  }

  deleteUser(id: string): Observable<IUserModel[]> {
    return this.http.delete<IUserModel[]>(this.baseUrl + '/' + id);
  }

  editUser(user: IUserModel, id: string): Observable<IUserModel[]> {
    return this.http.put<IUserModel[]>(this.baseUrl + '/' + id, user);
  }

  addUser(user: IUserModel): Observable<IUserModel> {
    return this.http.post<IUserModel>(this.baseUrl + '/', user);
  }

  getById(id: string): Observable<IUserModel> {
    return this.http.get<IUserModel>(this.baseUrl + '/' + id);
  }

  changeRole(user: IUserModel, id: string): Observable<IUserModel> {
    return this.http.put<IUserModel>(this.baseUrl + '/' + id, user);
  }

  changeConfirm(user: IUserModel, id: string): Observable<IUserModel> {
    return this.http.put<IUserModel>(this.baseUrl + '/' + id, user);
  }

  getUserPayload(): any {
    const token = this.localStorage.getItemValue<any>('token');
    if (token) {
      const decoded = jwt_decode(token.token);
      if (decoded) {
        const userPayload = decoded;
        return userPayload;
      } else {
        return '';
      }
    }
  }
}
