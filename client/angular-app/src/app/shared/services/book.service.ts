import { Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { IBookModel } from '@models/book.model';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  book: IBookModel = {
    _id: '',
    title: '',
    img: '',
    authorId: [],
    magazine: [],
    fullName: ''
  };

  private baseUrl = environment.apiBaseUrl + '/book';

  constructor(private http: HttpClient) {}

  addBook(book: IBookModel): Observable<IBookModel> {
    return this.http.post<IBookModel>(this.baseUrl, book);
  }

  getBooks(
    booksPerPage: number,
    currentPage: number
  ): Observable<IBookModel[]> {
    const queryParams = `?pagesize=${booksPerPage}&page=${currentPage}`;
    return this.http.get<IBookModel[]>(
      this.baseUrl + queryParams
    );
  }

  deleteBook(id: string): Observable<IBookModel> {
    return this.http.delete<IBookModel>(this.baseUrl + '/' + id);
  }

  getBookById(id: string): Observable<IBookModel[]> {
    return this.http.get<IBookModel[]>(this.baseUrl + '/' + id);
  }

  editBook(book: IBookModel, id: string): Observable<IBookModel> {
    const body: IBookModel = book;
    return this.http.put<IBookModel>(this.baseUrl + '/' + id, body);
  }

  searchBook(title: string): Observable<IBookModel[]> {
    const body = { title };
    return this.http.post<IBookModel[]>(this.baseUrl + '/search', body);
  }
}
