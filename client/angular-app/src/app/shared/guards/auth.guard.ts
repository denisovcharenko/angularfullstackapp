import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';
import { AuthService } from '@services/auth.service';
import { LocalStorageService } from '@services/localStorage.service';
import { UserService } from '@services/user.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router,
    private localStorageService: LocalStorageService,
    private userService: UserService
  ) {}

  canActivate(next: ActivatedRouteSnapshot): boolean {
    if (!this.authService.isLoggedIn()) {
      this.router.navigateByUrl('/login');
      this.localStorageService.deleteItem('token');
      return false;
    }

    const currentUser = this.userService.getUserPayload().userRole;
    const confirmUser = this.userService.getUserPayload().confirm;
    const confirm = next.data.confirm;
    const role = next.data.userRole;

    if (role.indexOf(currentUser) !== -1) {
      return true;
    }

    if (role.indexOf(currentUser) === -1) {
      this.router.navigateByUrl('/error-page');
      return false;
    }

    if (confirmUser !== confirm) {
      return true;
    }
    if (confirmUser === confirm) {
      this.router.navigateByUrl('/guest-profile');
      return false;
    }
    return false;
  }
}
