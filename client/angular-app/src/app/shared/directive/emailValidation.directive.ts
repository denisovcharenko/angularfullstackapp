import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[emailvalidator]'
})
export class EmailValidatorDirective {
  constructor(private formControl: NgControl) {}
  @HostListener('input') onMouseEnter() {
    this.formControl.control.setErrors({ emailvalidator: true });
    const isValid = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(
      this.formControl.value
    );
    if (!isValid) {
      return null;
    } else {
      return this.formControl.control.setErrors({ emailvalidator: false });
    }
  }
}
