import { Observable } from 'rxjs';

import {
    HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LocalStorageService } from '@services/localStorage.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private localStorageService: LocalStorageService) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (!(req.url.indexOf('auth/register') === -1)) {
      return next.handle(req);
    }
    if (!(req.url.indexOf('auth/login') === -1)) {
      return next.handle(req);
    }

    const paramReq = req.clone({
      headers: new HttpHeaders({
        'x-access-token': this.localStorageService.getItemValue<any>('token')
          .token
      })
    });
    return next.handle(paramReq);
  }
}
