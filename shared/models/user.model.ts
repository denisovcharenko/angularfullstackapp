export interface IUserModel {
    _id: any;
    email: string,
    fullName: string,
	password: string,
	userRole: number,
	confirm: boolean,
	userId: string
}